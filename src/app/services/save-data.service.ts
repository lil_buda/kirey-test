import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class SaveDataService {

  constructor() { }

  serviceData:object[];

  saveData(data) {
    this.serviceData = data;
  }
  
  getData() {
    return this.serviceData;
  }
}
