import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule }   from '@angular/forms';

// Components
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { TableComponent } from './table/table.component';

// Services
import { SaveDataService } from './services/save-data.service';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    TableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
    
  ],
  providers: [
    SaveDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
