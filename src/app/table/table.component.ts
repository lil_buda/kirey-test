import { Component, OnInit, EventEmitter } from '@angular/core';
import { SaveDataService } from '../services/save-data.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  
  data:Object[] = [];
  form:any;
  selectedObjectIndex:number;
  deleteModal:boolean = false;
  addModal:boolean = false;
  editModal:boolean = false;
  selectedName:string;

  constructor(
    private _saveDataService:SaveDataService
  ) { }
  

  handleModal(type?, index?, obj?) {
    switch(type) {
      case 'addModal':
      this.openAddModal();
      break;
      case 'deleteModal':
      this.openDeleteModal(index, obj);
      break;
      case 'editModal':
      this.openEditModal(index);
      break;
      default:
      break;
    }
    if(this.form) this.form.reset();
    document.getElementById("modal-div").classList.toggle("hide");
  }

  openAddModal() {
    this.deleteModal = false;
    this.editModal = false;
    this.addModal = true;
  }

  addData(form) {

    if(!this.validateData(form)) return;

    this.data.push(form.value);
    this.form = form;
    this.handleModal();
    this.saveData();
  }

  openDeleteModal(index, obj) {
    this.addModal = false;
    this.editModal = false;
    this.deleteModal = true;
    this.selectedName = obj.name;
    this.selectedObjectIndex = index;
   
  }

  deleteData(value) {
    if(value) {
      this.data.splice(this.selectedObjectIndex, 1);
    }
    this.handleModal();
    this.saveData();

  }

  openEditModal(index) {
    this.addModal = false;
    this.deleteModal = false;
    this.editModal = true;
    this.selectedObjectIndex = index;
  }

  editData(form) {
    
    if(!this.validateData(form)) return;

    this.data.splice(this.selectedObjectIndex, 1, form.value);
    this.handleModal();
    this.saveData();
  }

  closeModal() {
    document.getElementById("modal-div").classList.toggle("hide");
  }

  saveData() {
    this._saveDataService.saveData(this.data);
  }

  validateData(form) {
    if(!form.value.name) {
      alert("Name field cannot be empty.");
      return false;
    }
    if(!form.value.code) {
      alert("Code field is empty or isn't a number.");
      return false;
    }
    if(!form.value.duration) {
      alert("Duration field is empty or isn't a number.");
      return false;
    }
    if(!form.value.numOfAcc) {
      alert("Number of accounts field is empty or isn't a number.");
      return false;
    }
    if(!form.value.date) {
      alert("Date field cannot be empty.");
      return false;
    }
    return true;
  }

  ngOnInit() {
    let serviceData = this._saveDataService.getData();
    if(serviceData) this.data = this._saveDataService.getData();
  }

}
