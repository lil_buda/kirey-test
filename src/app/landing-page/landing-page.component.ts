import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  constructor() { }

  toggleMenu() {
    document.getElementById("menu-mobile-links").classList.toggle("hide");
  }

  scrollToElement(element): void {
    element.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
  }

  ngOnInit() {
    window.onscroll = () => {
      const nav = document.querySelector('#fixed-menu-wrapper');
      if(window.scrollY <= 20) nav.className = ''; else nav.className = 'scroll';
    };
  }
}
