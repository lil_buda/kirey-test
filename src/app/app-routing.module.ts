import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { TableComponent } from './table/table.component';

const routes: Routes = [
  { path: 'home', component: LandingPageComponent },
  { path: 'table', component: TableComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: LandingPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
